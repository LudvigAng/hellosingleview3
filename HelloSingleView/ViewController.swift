//
//  ViewController.swift
//  HelloSingleView
//
//  Created by Ludvig Angenfelt on 2019-10-22.
//  Copyright © 2019 Ludvig Angenfelt. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func pressed(_ sender: Any) {
        print("hello there")
    }
    
}

